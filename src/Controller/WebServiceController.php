<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\Categorie;
use App\Entity\Film;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

#[Route('/movies', name: 'app_movies', stateless: true)]
class WebServiceController extends AbstractController
{

    public function __construct(
            private SerializerInterface $serializer,
            private EntityManagerInterface $entityManager,
            private Filesystem $filesystem
        )
    {
    }

    #[Route('/web/service', name: 'app_web_service', stateless: true)]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/WebServiceController.php',
        ]);
    }

    #[Route('/', name: '_movies_all', methods: ['GET'], stateless: true)]
    public function getMoviesAll(Request $request) : Response {

        try {
            if ($request->query->has('query')) return $this->findMovies($request);;
        
            $contentType = $request->headers->get('content-type');

            
            $movies = array();
            if (in_array($contentType, ["application/hal+json", "application/hal+xml"])) {
                $movies["links"] = array(
                "href" => "/",
                "rel" => "movies",
                "type" => "GET"
                );
            }
            if (count(json_decode($this->serializer->serialize($this->entityManager->getRepository(Film::class)->findAll(), "json"))) < 1) throw new Exception("Pas de résultat de recherche", 204);
            

            foreach (json_decode($this->serializer->serialize($this->entityManager->getRepository(Film::class)->findAll(), "json")) as $m) {
                $movies[] = array(
                    "uuid" => $m->uid,
                    "name" => $m->nom,
                    "description" => $m->description,
                    "rate" => intval($m->note),
                    "duration" => $m->duration,
                    "createdAt" => $m->createdAt,
                    "updatedAt" => $m->updatedAt,
                );
            }
            return $this->handleResponse($contentType, (new Film), $movies);
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
        
    }

    #[Route('/{uid}', name: '_movies_page', methods: ['GET'], stateless: true)]
    public function getMoviesPage(Request $request, UuidV4 $uid) : Response {
        try {
            if (!preg_match('/\d+/', $uid))  throw new \Exception("Le film est inconnu", 404);

            $contentType = $request->headers->get('content-type');
            
            $movies = array();
            if (in_array($contentType, ["application/hal+json", "application/hal+xml"])) {
                $movies["links"] = array(
                "href" => "/" + $uid,
                "rel" => "movies",
                "type" => "GET"
                );
            }
            if (count(json_decode($this->serializer->serialize($this->entityManager->getRepository(Film::class)->findBy(["uid" => $uid]), "json"))) < 1) throw new Exception("erreur interne", 500);
            

            foreach (json_decode($this->serializer->serialize($this->entityManager->getRepository(Film::class)->findBy(["uid" => $uid]), "json")) as $m) {
                $movies[] = array(
                    "uuid" => $m->uid,
                    "name" => $m->nom,
                    "description" => $m->description,
                    "rate" => intval($m->note),
                    "duration" => $m->duration,
                    "createdAt" => $m->createdAt,
                    "updatedAt" => $m->updatedAt,
                );
            }
            return $this->handleResponse($contentType, (new Film), $movies);
            
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }
    
    #[Route('/', name: '_movies_post', methods: ['POST'], stateless: true)]
    public function setMovies(Request $request) : Response {
        try {
            if (empty($request->getContent())) throw new Exception("Le contenu de l'objet film dans le body est invalide", 422);
            $content = json_decode($request->getContent());
            $contentType = $request->headers->get('content-type');

            $this->verifResponce($content);


            $movie = new Film();
            $movie->setUid($content->uid);
            $movie->setNom($content->name);
            $movie->setDate(new DateTime());
            $movie->setDescription($content->description);
            $movie->setNote($content->rate);
            $movie->setDuration(new \DateInterval("PT" . $content->duration. "M"));
            $movie->setCreatedAt(new DateTime());
            $movie->setUpdatedAt(new DateTime());

            $this->entityManager->persist($movie);
            $this->entityManager->flush();

            return $this->handleResponse($contentType, new Film, array(
                "uid" => $movie->getUid(), 
                "name" => $movie->getNom(), 
                "description" => $movie->getDescription(), 
                "rate" => $movie->getNote(),
                "duration" => $movie->getDuration(),
                "createdAt" => $movie->getCreatedAt(), 
                "updatedAt" => $movie->getUpdatedAt()
            ), Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->json($e->getMessage());
        }
    }

    #[Route('/{uid}', name: '_movies_put', methods: ['PUT'], stateless: true)]
    public function updateMovies(Request $request, UuidV4 $uid) : Response {
        try {
            if (empty($request->getContent()) && (new UuidV4())->isValid($uid)) throw new Exception("Le contenu de l'objet film dans le body est invalide", 422);
            $content = json_decode($request->getContent());
            $contentType = $request->headers->get('content-type');
            
            $this->verifResponce($content);

            $movie = $this->entityManager->getRepository(Film::class)->findOneBy(["uid" => $uid]);

            if (empty($movie)) throw new Exception("Le film est inconnu", 404);
            

            $movie->setUid($content->uid);
            $movie->setNom($content->name);
            $movie->setDescription($content->description);
            $movie->setNote($content->rate);
            $movie->setUpdatedAt(new DateTime());

            $this->entityManager->persist($movie);
            $this->entityManager->flush();

            return $this->handleResponse($contentType, new Film, array(
                "uid" => $movie->getUid(), 
                "name" => $movie->getNom(), 
                "description" => $movie->getDescription(), 
                "rate" => $movie->getNote(), 
                "createdAt" => $movie->getCreatedAt(), 
                "updatedAt" => $movie->getUpdatedAt()
            ));
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{uid}', name: '_movies_delete', methods: ['DELETE'], stateless: true)]
    public function deleteMovies(Request $request, UuidV4 $uid) : Response {
        try {
            if ((new UuidV4())->isValid($uid)) throw new Exception("Le contenu de l'objet film dans le body est invalide", 422);
            $contentType = $request->headers->get('content-type');

            $movie = $this->entityManager->getRepository(Film::class)->findOneBy(["uid" => $uid]);

            if (empty($movie)) throw new Exception("Le film est inconnu", 404);
            
            $this->entityManager->remove($movie);
            $this->entityManager->flush();

            return $this->handleResponse($contentType, new Film, array(), Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    private function findMovies(Request $request) : Response {
        $query = $request->query->get('query');
        $contentType = $request->headers->get('content-type');

        return $this->handleResponse(
            $contentType,
            new Film,
            $this->entityManager->getRepository(Film::class)->select($query)
        );
    }


    

    // Fonction pour gérer la réponse en fonction du content-type
    private function handleResponse(string $contentType = "application/json", Film $film, $halfilm = null, ?int $codeReturn = 200): JsonResponse|Response
    {
        switch ($contentType) {
            case 'application/hal+json':
                return $this->json($halfilm, $codeReturn, ['content-type' => 'application/hal+json']);
            case 'application/json':
                if (empty($film))
                    return $this->json(json_decode($this->serializer->serialize($film, "json")), $codeReturn, ['content-type' => 'application/json']);
                else
                    return $this->json(json_decode($this->serializer->serialize($halfilm, "json")), $codeReturn, ['content-type' => 'application/json']);
            case 'application/hal+xml': 
                return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($halfilm, "json"), true), 'xml', ['xml_root_node_name' => 'films']), $codeReturn, ['content-type' => 'application/hal+xml']);
            case 'application/xml':
                if (empty($film)) return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($film, "json"), true), 'xml', ['xml_root_node_name' => 'films']), $codeReturn, ['content-type' => 'application/xml']);
                else return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($halfilm, "json"), true), 'xml', ['xml_root_node_name' => 'films']), $codeReturn, ['content-type' => 'application/xml']);
            default:
                return $this->json(['erreur' => "Le content-type doit être 'application/json' 'application/hal+json' 'application/hal+xml' ou 'application/xml'"], 500);
        }
    }

    private function verifResponce(mixed $content) : void {
        if (
            (empty($content->uid) && empty($content->name) && empty($content->description) && empty($content->rate) && empty($content->duration)) && 
            (strlen($content->name) > 128 && strlen($content->description) > 4096 && ($content->rate < 0 && $content->rate > 5) && ($content->duration < 0 && $content->duration > 240))) {
            new Exception("Le contenu de l'objet film dans le body est invalide", 422);
        }
    }

}
